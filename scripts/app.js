/**
 * @author Santiago Luna
 * 2032367
 * @Date 11/8/2021
 * @summary this program fetches a json object from the nasa api and prints a description and an image to the user
 */
"use strict";

document.addEventListener("DOMContentLoaded", setup);
const links = new Map();

/**
 * @author Santiago Luna
 * @function adding event listener to the button
 */
function setup() {
  let button = document.querySelector("button");
  button.addEventListener("click", submit);
}

/**
 * @author Santiago Luna
 * @function calls the buildURL function to create the url object and passes it
 * into the getJson function
 */
function submit() {
  let url = buildURL();
  getJSON(url, addPara);
}

/**
 * @author Santiago Luna
 * @function creates a url object using a parameter object
 */
function buildURL() {
  let urlStart = new URL("https://api.nasa.gov/planetary/apod");

  let paramObj = {
    count: "1",
    api_key: "Ztj9qcQ8943FHFjPIQaDSdXTBEfW3Hiuan7WTEfa",
  };

  let params = new URLSearchParams(paramObj);
  let url = new URL(urlStart + "?" + params);
  return url;
}

/**
 * @author Santiago Luna
 * @param uri it is the url object.
 * @param action this is the callback function to be executed on the fetched json
 * @function using the url the, a JSON object is fetched then
 * the callback function is called on the json object.
 * If the fetch fails then an error is thrown and the user is promted to try again.
 */
function getJSON(uri, action) {
  let p = document.querySelector("p").textContent;

  fetch(uri)
    .then((response) => {
      if (!response.ok) {
        p = "";
        window.alert("AN ERROR OCCURED: try again");
        throw new Error(`ERROR!`);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      action(json);
      updateMap(json);
    })
    .catch((err) => console.log(err));
}

/**
 * @author Santiago Luna
 * @param json the json is passed so that it can update the corresponding key in the map with the JSON and use its values.
 * @function inserts the JSON object into the map uses a callback to create the buttons,
 * if there has been more than 5 fetches then the next one will call a callback.
 */
function updateMap(json) {
  let counter = links.size - 1;
  if (counter < 4) {
    counter++;
    links.set(counter, json[0]);
    //passing json object stored in map
    addButton(links.get(counter).title, counter);
  } else {
    arrangeButtons(json[0]);
  }
}

/**
 * @author Santiago Luna
 * @param json the JSON object to use its values
 * @function grabs all the buttons and assigns the corresponding map item with the matching key to the button
 * then updates the what the buttons will show to the screen.
 *
 */
function arrangeButtons(json) {
  let buttons = document.querySelectorAll("[id^=btn]");
  links.forEach((value, key) => {
    links.set(key, links.get(key + 1));
  });
  links.set(4, json);
  buttons.forEach((btn, idx) => (btn.textContent = links.get(idx).title));
}

/**
 * @author Santiago Luna
 * @param title the title in the JSON object
 * @param id the postition of the JSON object in the map
 * @function creates the button elements and appends them to the dom,
 * adding the event listeners so when clicked they show the information of the corresponding JSON object.
 */
function addButton(title, id) {
  let button = document.createElement("button");
  let image = document.querySelector("img");
  let p = document.querySelector("p");
  let container = document.querySelector(".buttons");
  button.setAttribute("id", `btn-${id}`);
  button.textContent = title;
  container.appendChild(button);
  button.addEventListener("click", (e) => {
    const id = Number(e.target.id.split("-")[1]);
    image.src = links.get(id).hdurl;
    p.textContent = links.get(id).explanation;
  });
}

/**
 * @author Santiago Luna
 * @param json using its values
 * @function grabs the JSON object and uses the description and hdurl to upate the paragraph and image element.
 */
function addPara(json) {
  //extract json object from array
  let data = json[0];
  let p = document.querySelector("p");
  let image = document.querySelector("img");

  p.textContent = data.explanation;
  image.src = data.hdurl;
}
